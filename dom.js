

//WE ARE LEARNING TO CONSUME APIS FROM ANY WEBSITE
//FIRST READ FETCH FUNCTION ON MDN


const TODOIST_TOKEN = "a466e89d5ef5a320e2d87b5a3a1589514d9f25c2";

const container = document.querySelector(".container");


function fetchActiveTasks() {
    return fetch("https://api.todoist.com/rest/v1/tasks", {
      headers: {
        Authorization: `Bearer ${TODOIST_TOKEN}`,
      },
    }).then((res) => res.json());
  }


  function createTodoistTaskElem(task) {   // task is json array of new task
    const card = document.createElement("div");
    const cardBody = document.createElement("div");
    card.classList.add("card");
    cardBody.classList.add("card-body");
    const contentElem = document.createElement("p");
    contentElem.textContent = task.content;
    cardBody.appendChild(contentElem);
    card.appendChild(cardBody);           //card -> card-body -> p ->task.content
    return card;
  }


  function displayActiveTasks() {

    fetchActiveTasks()
      .then((tasks) => {                           //tasks is a json array of all active tasks (objects)
        return tasks.map(createTodoistTaskElem);
      })
      .then((cardElems) => {                       //cardElems is a json array of all cards
        cardElems.forEach((card) => {
          container.appendChild(card);
        });
      });
  
  
  }
  
  displayActiveTasks();
  

  //Till now i am only displaying previous items of my todoist on my webpage.


//-------------------------------------------------------------------------------------------------------------------



//Now i will make it function to be able to add new items on my webpage and todoist simultaneously.



const addTaskFormElem = document.getElementById("add-task");  //form fetched


addTaskFormElem.addEventListener("submit", function (event) {
  event.preventDefault();
  const inputElem = addTaskFormElem.querySelector("input");  //tag selector
  const content = inputElem.value;

  createTask(content).then((newTask) => {       //content is sent to createTask which is returning it as json array of new task.
    inputElem.value = "";
    const newTaskElem = createTodoistTaskElem(newTask);
    container.appendChild(newTaskElem);
  });
});
                                                                
function createTask(content) {                                  //fetch returns a promise
  return fetch("https://api.todoist.com/rest/v1/tasks", {
    method: "POST",
    headers: {
      Authorization: `Bearer ${TODOIST_TOKEN}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      content: content,
    }),
  }).then((res) => {
      
    return res.json();           

}); 
}

// json() method returns a promise so "then" is used again above to collect the actual result.










